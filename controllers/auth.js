auth = {
    async checkAuth (ctx, next) {
        if (!ctx.session || !ctx.session.userId) {
            ctx.redirect('/signin');
        }
        await next()
    },
    async checkAuthSignedIn(ctx, next) {
        if (ctx.session.userId && (ctx.path == '/signin' || ctx.path == '/signup')) {
            ctx.redirect('/');
        }
        await next();
    },
    async checkAuthMiddleware (ctx, next) {
        if  ((!ctx.session || !ctx.session.userId) && (ctx.path == '/' || ctx.path == '/create')) {
            ctx.redirect('/signin');
        }
        else if (ctx.session.userId && (ctx.path == '/signin' || ctx.path == '/signup')) {
            ctx.redirect('/');
        }
        await next();
    }
};

module.exports = auth;