const uuidv4 = require('uuid/v4');
const fs = require('fs-extra');
const path = require('path');
const pool = require('./database');

const pictureDir = path.join(__dirname, '..', 'public', 'images');
const allowFileType = {
  'image/png': true,
  'image/jpeg': true
};
const uploadHandler = async ctx => {
    try {
      // check allow file type
      if (!allowFileType[ctx.request.files.photo.type]) {
        throw new Error('file type not allow');
      }
      // form datas
      console.log(ctx.request.body.caption);
      console.log(ctx.request.body.detail);
      // file datas
      console.log(ctx.request.files.photo.name);
      console.log(ctx.request.files.photo.path);
      console.log(ctx.request.files.photo.name.slice(ctx.request.files.photo.name.lastIndexOf('.'), ));
      const fileName = uuidv4() + ctx.request.files.photo.name.slice(ctx.request.files.photo.name.lastIndexOf('.'), ); // generate uuid for file name
      const [result, fields] = await pool.query(`
      INSERT INTO pictures (
          id, caption, created_by
      ) VALUES (
          ?, ?, ?
      )
  `, [fileName, ctx.request.body.caption, ctx.session.userId]);
      // move uploaded file from temp dir to destination
      await fs.rename(ctx.request.files.photo.path, path.join(pictureDir, fileName));
      ctx.redirect('/');
    } catch (err) {
      // handle error here
      ctx.status = 400;
      ctx.body = err.message;
      // remove uploaded temporary file when the error occurs
      fs.remove(ctx.request.files.photo.path);
    }
  }

  module.exports = uploadHandler;