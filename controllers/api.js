const pool = require('./database');
const bcrypt = require('bcrypt');
const uuidv4 = require('uuid/v4');
const fs = require('fs-extra');
const path = require('path');

module.exports = {
    async apiSignup(ctx) {
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/).test(ctx.request.body.email))
            {
                ctx.status = 400;
                return ctx.body = {
                    "error": "invalid email"
                };
            }

        if (ctx.request.body.password.length < 3) {
            ctx.status = 400;
                return ctx.body = {
                    "error": "password too short"
                };
        }

        const [result] = await pool.query(`
            SELECT * FROM users WHERE email = ?`, [ctx.request.body.email]);
        console.log(result.length);
        if (result.length > 0) {
            ctx.status = 400;
            return ctx.body = {
                "error": "email already used"
            };
        }

        const hashedPassword = await bcrypt.hash(ctx.request.body.password, 10);
        await pool.query(`
            INSERT INTO users (email, password) VALUES (?, ?)`, [ctx.request.body.email, hashedPassword]);
        const [[row]] = await pool.query(`
            SELECT id FROM users WHERE email = ?`, [ctx.request.body.email]);
        const userId = row['id'];
        return ctx.body = {
            "userId": userId
        };
    },
    async apiSignin(ctx) {
        const [[result]] = await pool.query(`
            SELECT * FROM users WHERE email = ?`, [ctx.request.body.email]);
        if (result &&  result['email'] === ctx.request.body.email && await bcrypt.compare(ctx.request.body.password, result.password)) {
            ctx.session.userId = result.id;
            console.log(ctx.session.userId);
            return ctx.body = {};
        } else {
            ctx.status = 400;
            return ctx.body = {
                "error": "wrong email or password"
            };
        }
    },
    async apiSignout(ctx) {
        ctx.session.userId = null;
        return ctx.body = {};
    },
    async getPikka(ctx) {
        const [result] = await pool.query(`
            SELECT pictures.id, pictures.caption, pictures.created_at, users.email, likes.user_id
            FROM users
            INNER JOIN pictures ON users.id = pictures.created_by
            LEFT JOIN likes ON pictures.id = likes.picture_id AND users.id = likes.user_id
            LEFT JOIN comments ON pictures.id = comments.picture_id
            `);
        const [[comments]] = await pool.query(`
            SELECT COUNT(*) FROM pictures 
            INNER JOIN comments 
            ON pictures.id = comments.picture_id
            `);
        const [[likes]] = await pool.query(`
        SELECT COUNT(*) FROM pictures 
        INNER JOIN likes
        ON pictures.id = likes.picture_id
        `);
        let arr = [];
        let obj = {};
        for (i in result) {
            obj = {
                id: result[i]['id'],
                caption: result[i]['caption'],
                picture: "localhost:8000/images/" + result[i]['id'],
                createdAt: result[i]['created_at'],
                commentCount: comments['COUNT(*)'],
                liked: Boolean(result[i]['userid']),
                likeCount: likes['COUNT(*)'],
                createdBy: result[i]['email']
            }
            arr.push(obj);
        }

        console.log(obj);

        ctx.set("Content-Type", "application/json; charset=utf-8")
        return ctx.body = JSON.stringify(arr);
    },
    async postPikka(ctx) {
        const pictureDir = path.join(__dirname, '..', 'public', 'images');
        const allowFileType = {
            'image/png': true,
            'image/jpeg': true
        };
        try {
            ctx.set("Content-Type", "application/json; charset=utf-8")
            if (!ctx.session || !ctx.session.userId) {
                ctx.status = 401;
                return {
                    "error": "unauthorized"
                };
            }

            if (!ctx.request.files) {
                ctx.status = 400;
                return {
                    "error": "picture required"
                };
            }

            if (ctx.request.body.caption == null) {
                ctx.status = 400;
                return {
                    "error": "caption required"
                };
            }

            if (!allowFileType[ctx.request.files.photo.type]) {
                throw new Error('file type not allow');
            }
            // form datas
            console.log(ctx.request.body.caption);
            console.log(ctx.request.body.detail);
            // file datas
            console.log(ctx.request.files.photo.name);
            console.log(ctx.request.files.photo.path);
            console.log(ctx.request.files.photo.name.slice(ctx.request.files.photo.name.lastIndexOf('.'), ));
            const fileName = uuidv4() + ctx.request.files.photo.name.slice(ctx.request.files.photo.name.lastIndexOf('.'), ); // generate uuid for file name
            await pool.query(`
                INSERT INTO pictures (
                    id, caption, created_by
                ) VALUES (
                    ?, ?, ?
                )
                `, [fileName, ctx.request.body.caption, ctx.session.userId]);
            const [[result]] = await pool.query(`SELECT * FROM pictures WHERE id = ?`, [fileName]);
            // move uploaded file from temp dir to destination
            await fs.rename(ctx.request.files.photo.path, path.join(pictureDir, fileName));
            return JSON.stringify({id: fileName, createdAt: result['created_at']});
        } catch (err) {
            // handle error here
            ctx.status = 400;
            ctx.body = err.message;
            // remove uploaded temporary file when the error occurs
            fs.remove(ctx.request.files.photo.path);
          }
    }
}