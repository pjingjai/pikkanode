const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const path = require('path');
const render = require('koa-ejs');
const koaBody = require('koa-body');
const session = require('koa-session');
const cors = require('@koa/cors');

const pool = require('./database');
const {
    rootGetHandler
} = require('./getHandlers');
const {
    signinGetHandler
} = require('./getHandlers');
const {
    signinPostHandler
} = require('./postHandlers');
const {
    signupGetHandler
} = require('./getHandlers');
const {
    signupPostHandler
} = require('./postHandlers');
const {
    createGetHandler
} = require('./getHandlers');
const uploadHandler = require('./upload');
const {
    checkAuthMiddleware
} = require('./auth');
const {FbGetHandler} = require('./getHandlers');
const {FbPostHandler} = require('./postHandlers');
const {FbBtnPostHandler} = require('./postHandlers');
const {apiSignup, apiSignin, apiSignout, getPikka, postPikka} = require('./api');

const app = new Koa();
const router = new Router();

render(app, {
    root: path.join(__dirname, '..', '/views'),
    layout: false,
    viewExt: 'ejs',
    cache: false
});
const flash = async (ctx, next) => {
    if (!ctx.session) throw new Error('flash message required session');
    ctx.flash = ctx.session.flash;
    delete ctx.session.flash;
    await next();
};

app.use(serve(path.join(__dirname, '..', 'public')));

router.get('/', rootGetHandler);

// router.get('/signin', signinGetHandler);
router.get('/signin', FbGetHandler);
router.post('/signin', koaBody({
    multipart: true
}), signinPostHandler);
router.post('/signupFb', koaBody({
    multipart: true
}), FbBtnPostHandler);

router.get('/signup', signupGetHandler);
router.post('/signup', koaBody({
    multipart: true
}), FbPostHandler);

router.get('/create', createGetHandler);
router.post('/create', koaBody({
    multipart: true
}), postPikka);

router.post('/api/v1/auth/signup', koaBody(), apiSignup);
router.post('/api/v1/auth/signin', koaBody(), apiSignin);
router.post('/api/v1/auth/signout', koaBody(), apiSignout);
router.get('/api/v1/pikka', getPikka);
router.post('/api/v1/pikka', koaBody({multipart: true}), postPikka);

app.keys = ['supersecret'];
const sessionConfig = {
    key: 'sess',
    maxAge: 10 * 1000 * 60,
    httpOnly: true
};

app.use(cors({
    origin: '*'
   }));
app.use(session(sessionConfig, app));
app.use(flash);
app.use(checkAuthMiddleware);
app.use(router.routes());
app.use(router.allowedMethods());
app.listen(8000);