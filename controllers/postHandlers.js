const pool = require('./database');
const bcrypt = require('bcrypt');

module.exports = {
    async signinPostHandler(ctx) {
        if (!ctx.request.body.username) {
            ctx.session.flash = { error: 'username required' };
            return ctx.redirect('/signin');
        }

        const [[result]] = await pool.query(`
            SELECT * FROM user WHERE email = ?`, [ctx.request.body.username]);
        if (result && result.password) {
            const hashedPassword = result.password;
            console.log(hashedPassword);
            const same = await bcrypt.compare(ctx.request.body.password, hashedPassword);

            if (!same) {
                console.log('wrong password');
                ctx.session.flash = { error: 'wrong password' };
                ctx.status = 303;
                ctx.redirect('/signin');
            } else {
                console.log('password correct');
                ctx.session.userId = result['user_id'];
                console.log(ctx.session.userId);
                ctx.redirect('/');
            }
        } else {
            ctx.session.flash = { error: 'not registered' };
            return ctx.redirect('/signin');
        }
    },
    async facebookLogin(ctx) {
        console.log('facebook login');
        ctx.session.userId = result.id;
        ctx.redirect('/');
    },
    async signupPostHandler(ctx) {
        console.log(ctx.request.body);
        const hashedPassword = await bcrypt.hash(ctx.request.body.password, 10);
        console.log(hashedPassword);
        const [result, fields] = await pool.query(`
            INSERT INTO users (
                email, password
            ) VALUES (
                ?, ?
            )
        `, [ctx.request.body.username, hashedPassword]);
        ctx.status = 303;
        ctx.redirect('/signin');
    },
    async FbPostHandler(ctx) {
        console.log(ctx.request.body);
        const hashedPassword = await bcrypt.hash(ctx.request.body.password, 10);
        console.log(hashedPassword);
        let username = ctx.request.body.username.slice(0, ctx.request.body.username.search('@'));
        const [result, fields] = await pool.query(`
            INSERT INTO user (
                email, password, username
            ) VALUES (
                ?, ?, ?
            )
        `, [ctx.request.body.username, hashedPassword, username]);
        ctx.status = 303;
        ctx.redirect('/signin');
    },
    async FbBtnPostHandler(ctx) {
        console.log(ctx.request.headers);
        console.log(ctx.request.body);
        const [[db]] = await pool.execute(`SELECT facebook_user_id FROM user WHERE facebook_user_id = ?`, [ctx.request.body['id']]);
        if (!db || !db['facebook_user_id']) {
            await pool.execute(`
                INSERT INTO user
                    (facebook_user_id, firstname, lastname, email)
                VALUES
                    (?, ?, ?, ?)
                `,[ctx.request.body['id'], ctx.request.body['first_name'], ctx.request.body['last_name'], ctx.request.body['email']]);
        }
        const [[row]] = await pool.execute(`
                SELECT user_id FROM user
                WHERE facebook_user_id = ?
                `,[ctx.request.body['id']]);
        ctx.session.userId = row['user_id'];
        console.log(ctx.session.userId);
        ctx.status = 303;
        return ctx.redirect('/');
    }
}