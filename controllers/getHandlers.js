const pool = require('./database');

module.exports = {
    async signinGetHandler(ctx) {
        await ctx.render('signin', {flash: ctx.flash});
    },
    async FbGetHandler(ctx) {
        await ctx.render('signinFb', {flash: ctx.flash});
    },
    async signupGetHandler(ctx) {
        await ctx.render('signup');
    },
    async createGetHandler(ctx) {
        await ctx.render('create');
    },
    async rootGetHandler(ctx) {
        const [result, fields] = await pool.query('SELECT * FROM pictures');
        const [picRows] = await pool.query(`SELECT * FROM pictures`);
        const [postRows] = await pool.query(`SELECT * FROM pictures INNER JOIN users ON pictures.created_by = users.id`);
        const [likeRows] = await pool.query(`SELECT COUNT(*) FROM likes INNER JOIN pictures ON likes.picture_id = pictures.id`);
        const [commentRows] = await pool.query(`SELECT COUNT(*) FROM comments INNER JOIN pictures ON comments.picture_id = pictures.id`);

        await ctx.render('main', {
            rows: result,
            fields: fields,
            img: picRows,
            postBy: postRows,
            likes: likeRows,
            comments: commentRows
        });
    }
}